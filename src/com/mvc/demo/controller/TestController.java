package com.mvc.demo.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 * @Description: 
 * @author: chenghengchao
 * @date: 2017年11月20日
 */
import org.springframework.web.bind.annotation.ResponseBody;

import com.mvc.demo.model.Room;
import com.mvc.demo.service.RoomManageService;
@Controller
@RequestMapping("/100inn")
public class TestController {
	@Autowired
	HttpServletRequest request;
	@Autowired
	RoomManageService roomManageService;
	@RequestMapping("/index")
	public String index(){
		return "index";
	}
	/**
	 * @Title: order 
	 * @Description: TODO
	 * @return
	 * @return: String
	 */
	@RequestMapping("/orderUI")
	public String order(){
		return "orderUI";
	}
	@RequestMapping("/stateUI")
	public String state(){
		return "stateUI";
	}
	@RequestMapping("/roomUI")
	public String room(){
		List<Room> roomList = roomManageService.findAll();
		request.setAttribute("roomList", roomList);
		return "roomUI";
	}
	@RequestMapping("/goodUI")
	public String good(){
		return "goodUI";
	}
	@RequestMapping("/addSave")
	public void addSave(){
		String roomId = request.getParameter("roomId");
		String roomType = request.getParameter("roomType");
		String roomDesc = request.getParameter("roomDesc");
		Room room = new Room();
		room.setRoomId(Integer.valueOf(roomId));
		room.setRoomType(roomType);
		room.setRoomState(0);//默认0：空房
		room.setRoomDesc(roomDesc);
		roomManageService.addRoom(room);
	}
	
	@RequestMapping("/showAll")
	@ResponseBody
	public List<Room> showAll(){
		List<Room> roomList = roomManageService.findAll();
		request.setAttribute("roomList", roomList);
		return roomManageService.findAll();
	}
	
}
