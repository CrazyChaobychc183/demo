package com.mvc.demo.dao;

import java.util.List;

import com.mvc.demo.model.Room;

public interface RoomManageDao {
	//添加
	void add(Room room);
	//查
	List<Room> selectAll();
	//按id查
	Room selectById(String id);
	//删除
	void delete(String[] id);
	//改
	void updateById(Room room);
}
