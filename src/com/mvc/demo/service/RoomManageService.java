package com.mvc.demo.service;

import java.util.List;

import com.mvc.demo.model.Room;

public interface RoomManageService {
	void addRoom(Room room);
	List<Room> findAll();
	Room findOne(String id);
}
