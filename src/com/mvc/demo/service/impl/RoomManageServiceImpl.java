package com.mvc.demo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mvc.demo.dao.RoomManageDao;
import com.mvc.demo.model.Room;
import com.mvc.demo.service.RoomManageService;
@Service
public class RoomManageServiceImpl implements RoomManageService {
	@Autowired
	RoomManageDao roomManageDao;
	@Override
	public void addRoom(Room room) {
		roomManageDao.add(room);
	}
	@Override
	public List<Room> findAll() {
		return roomManageDao.selectAll();
	}
	@Override
	public Room findOne(String id) {
		return roomManageDao.selectById(id);
	}

}
