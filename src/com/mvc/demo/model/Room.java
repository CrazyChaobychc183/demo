package com.mvc.demo.model;

public class Room {
	private int roomId;
	private String roomType;
	private int roomState;
	private String roomDesc;
	public Room() {
		super();
	}
	public Room(int roomId, String roomType, int roomState, String roomDesc) {
		super();
		this.roomId = roomId;
		this.roomType = roomType;
		this.roomState = roomState;
		this.roomDesc = roomDesc;
	}
	public int getRoomId() {
		return roomId;
	}
	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}
	public String getRoomType() {
		return roomType;
	}
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}
	public int getRoomState() {
		return roomState;
	}
	public void setRoomState(int roomState) {
		this.roomState = roomState;
	}
	public String getRoomDesc() {
		return roomDesc;
	}
	public void setRoomDesc(String roomDesc) {
		this.roomDesc = roomDesc;
	}
	@Override
	public String toString() {
		return "Room [roomId=" + roomId + ", roomType=" + roomType + ", roomState=" + roomState + ", roomDesc="
				+ roomDesc + "]";
	}
	
}
